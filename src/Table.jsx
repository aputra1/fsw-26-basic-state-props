
const Table = ({
    books = [],
    onChange
}) => {

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>
                    {books.map((data, index) => (
                        <tr>
                            <td>{index + 1}</td>
                            <td>{data.nama}</td>
                            <td>{data.harga}</td>
                        </tr>
                    ))}
                    </tbody>
            </table>
            <button onClick={onChange}>Ubah State</button>
        </div>
    )
}

export default Table